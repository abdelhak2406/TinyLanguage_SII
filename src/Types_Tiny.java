/*
Classe qui contient les types existant dans notre language
*/

public enum Types_Tiny {
    INTEGER,
    FLOAT,
    STRING;

}
